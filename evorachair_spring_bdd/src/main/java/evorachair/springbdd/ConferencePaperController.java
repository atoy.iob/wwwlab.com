package evorachair.springbdd;

import java.awt.print.Paper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/conferences/{conferenceId}/papers")
public class ConferencePaperController<PaperService, ConferenceService> {

    @Autowired
    public ConferencePaperController(PaperService paperService, ConferenceService conferenceService) {
    }

    @PostMapping("/accept")
    public ResponseEntity<String> acceptPaper(@PathVariable Long conferenceId, @RequestBody Paper paper) {
    
        // Return a success response
        return ResponseEntity.status(HttpStatus.CREATED).body("Paper accepted for conference successfully");
    }
}
