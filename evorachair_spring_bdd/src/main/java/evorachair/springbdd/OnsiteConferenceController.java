package evorachair.springbdd;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/onsite-conferences")
public class OnsiteConferenceController<OnsiteConferenceService> {

    private final OnsiteConferenceController<?> onsiteConferenceService;

    @Autowired
    public OnsiteConferenceController(OnsiteConferenceService onsiteConferenceService) {
        this.onsiteConferenceService = (OnsiteConferenceController<?>) onsiteConferenceService;
    }

    @PostMapping
    public <OnsiteConference> ResponseEntity<OnsiteConference> createOnsiteConference(@RequestBody OnsiteConference conference) {
        @SuppressWarnings("unchecked")
		OnsiteConference createdConference = (OnsiteConference) onsiteConferenceService.createOnsiteConference(conference);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdConference);
    }

    @GetMapping
    public <OnsiteConference> ResponseEntity<List<OnsiteConference>> getAllOnsiteConferences() {
        @SuppressWarnings("unchecked")
		List<OnsiteConference> conferences = (List<OnsiteConference>) onsiteConferenceService.getAllOnsiteConferences();
        return ResponseEntity.ok(conferences);
    }

    @GetMapping("/{id}")
    public <OnsiteConference> ResponseEntity<OnsiteConference> getOnsiteConferenceById(@PathVariable Long id) {
        @SuppressWarnings("unchecked")
		OnsiteConference conference = (OnsiteConference) onsiteConferenceService.getOnsiteConferenceById(id);
        if (conference == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(conference);
    }

    @PutMapping("/{id}")
    public <OnsiteConference> ResponseEntity<OnsiteConference> updateOnsiteConference(@PathVariable Long id, @RequestBody OnsiteConference conference) {
        @SuppressWarnings("unchecked")
		OnsiteConference updatedConference = (OnsiteConference) onsiteConferenceService.updateOnsiteConference(id, conference);
        if (updatedConference == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedConference);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteOnsiteConference(@PathVariable Long id) {
        boolean conferenceDeleted = onsiteConferenceService.deleteOnsiteConference(id) != null;
        if (conferenceDeleted) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
