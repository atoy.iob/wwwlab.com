package evorachair_spring_bdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Creating_ConferenceStepDefinition {

	@Given("I am a conference reviewer")
	public void i_am_a_conference_reviewer() {
	}
	@Given("there are submitted papers for review")
	public void there_are_submitted_papers_for_review() {

	}

	@When("I review the papers")
	public void i_review_the_papers() {

	}

	@Then("I can provide feedback and make acceptance decisions")
	public void i_can_provide_feedback_and_make_acceptance_decisions() {

	}

	@Given("I am reviewing a paper")
	public void i_am_reviewing_a_paper() {

	}

	@When("I provide feedback on the paper")
	public void i_provide_feedback_on_the_paper() {

	}

	@Then("the feedback is recorded")
	public void the_feedback_is_recorded() {

	}

	@Given("I have reviewed all the papers")
	public void i_have_reviewed_all_the_papers() {

	}

	@When("I make acceptance decisions")
	public void i_make_acceptance_decisions() {

	}

	@Then("the decisions are recorded")
	public void the_decisions_are_recorded() {

	}

}
