Feature: Conference Program
  As a conference organizer
  I want to generate a conference program
  So that participants can see the schedule of events

  Scenario: Generating Conference Program
    Given I am a conference organizer
    And the conference has accepted papers
    When I generate the conference program
    Then the program is generated successfully

  Scenario: Adding Keynote Speakers
    Given the conference program is generated
    And there are keynote speakers scheduled
    When I add the keynote speakers to the program
    Then the program is updated with the keynote speakers

  Scenario: Scheduling Sessions
    Given the conference program is generated
    And the sessions are planned
    When I schedule the sessions in the program
    Then the program is updated with the scheduled sessions

  Scenario: Publishing Conference Program
    Given the conference program is generated and updated
    When I publish the conference program
    Then the program is made available to participants
