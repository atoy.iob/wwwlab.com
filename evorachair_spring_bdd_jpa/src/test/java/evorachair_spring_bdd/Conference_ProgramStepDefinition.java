package evorachair_spring_bdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Conference_ProgramStepDefinition {

    @Given("I am a conference organizer")
    public void iAmConferenceOrganizer() {
        // Implementasi langkah-langkah di sini
    }

    @Given("the conference has accepted papers")
    public void conferenceHasAcceptedPapers() {
        // Implementasi langkah-langkah di sini
    }

    @When("I generate the conference program")
    public void generateConferenceProgram() {
        // Implementasi langkah-langkah di sini
    }

    @Then("the program is generated successfully")
    public void programGeneratedSuccessfully() {
        // Implementasi langkah-langkah di sini
    }

    @Given("the conference program is generated")
    public void conferenceProgramGenerated() {
        // Implementasi langkah-langkah di sini
    }

    @Given("there are keynote speakers scheduled")
    public void keynoteSpeakersScheduled() {
        // Implementasi langkah-langkah di sini
    }

    @When("I add the keynote speakers to the program")
    public void addKeynoteSpeakersToProgram() {
      
    }

    @Then("the program is updated with the keynote speakers")
    public void programUpdatedWithKeynoteSpeakers() {
      
    }

    @Given("the sessions are planned")
    public void sessionsPlanned() {
      
    }

    @When("I schedule the sessions in the program")
    public void scheduleSessionsInProgram() {
       
    }

    @io.cucumber.java.en.Then("the program is updated with the scheduled sessions")
    public void programUpdatedWithScheduledSessions() {
       
    }

    @Given("the conference program is generated and updated")
    public void conferenceProgramGeneratedAndUpdated() {
        
    }

    @When("I publish the conference program")
    public void publishConferenceProgram() {
       
    }

    @Then("the program is made available to participants")
    public void programMadeAvailableToParticipants() {
       
    }

}