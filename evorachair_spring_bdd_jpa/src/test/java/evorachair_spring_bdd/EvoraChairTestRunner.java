package evorachair_spring_bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:evorachair.feature",
        glue = "com.example.evorachair",
        plugin = {"pretty", "html:target/cucumber-reports"}
)
public class EvoraChairTestRunner {
}
