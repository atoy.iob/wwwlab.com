package evorachair_spring_bdd;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
public class Call_for_PapersStepDefinition {
	@Given("I am a conference organizer")
	public void i_am_a_conference_organizer() {
   
	}
	@Given("there are accepted papers for notification")
	public void there_are_accepted_papers_for_notification() {
	    
	}

	@When("I send acceptance notifications to authors")
	public void i_send_acceptance_notifications_to_authors() {
	   
	}

	@Then("the authors are notified about the acceptance")
	public void the_authors_are_notified_about_the_acceptance() {
	    
	}

	@Given("acceptance notifications are sent")
	public void acceptance_notifications_are_sent() {
	    
	}

	@When("authors receive the acceptance notifications")
	public void authors_receive_the_acceptance_notifications() {
	   
	}

	@Then("the acceptance details are provided to authors")
	public void the_acceptance_details_are_provided_to_authors() {
	    
	}

	@Given("there are rejected papers")
	public void there_are_rejected_papers() {
	    
	}

	@When("I send rejection notifications to authors")
	public void i_send_rejection_notifications_to_authors() {
	    
	}

	@Then("the authors are notified about the rejection")
	public void the_authors_are_notified_about_the_rejection() {
	    
	}	

}
