package evorachair.springbdd;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Accepting_Paper {

    @io.cucumber.java.en.Given("^there is a submitted paper$")
    public void thereIsSubmittedPaper() {
        // Implementation of steps goes here
    }

    @When("^I accept the paper$")
    public void acceptPaper() {
        // Implementation of steps goes here
    }

    @Then("^the paper is marked as accepted$")
    public void paperMarkedAsAccepted() {
        // Implementation of steps goes here
    }

    @Then("^the paper is included in the conference program$")
    public void paperIncludedInConferenceProgram() {
        // Implementation of steps goes here
    }
}