package evorachair.springbdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvoraChairApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvoraChairApplication.class, args);
    }

}
