package evorachair.springbdd;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpHeaders;
import java.nio.charset.StandardCharsets;

public class PaperControllerCallExample {

    public static void main(String[] args) throws IOException, InterruptedException {
        // Create an instance of HttpClient
        HttpClient httpClient = HttpClient.newHttpClient();

        // Define the endpoint URL
        String endpointUrl = "http://localhost:8080/api/papers/accept";

        // Create a JSON payload for the paper
        String requestBody = "{\"title\": \"Sample Paper\", \"author\": \"John Doe\"}";

        // Create an HttpRequest with the POST method and the JSON payload
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(endpointUrl))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody, StandardCharsets.UTF_8))
                .build();

        // Send the HTTP request and get the response
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        // Print the response status code
        int statusCode = response.statusCode();
        System.out.println("Response status code: " + statusCode);

        // Print the response body
        String responseBody = response.body();
        System.out.println("Response body: " + responseBody);

        // Print the response headers
        HttpHeaders headers = response.headers();
        System.out.println("Response headers: " + headers.map());
    }
}
