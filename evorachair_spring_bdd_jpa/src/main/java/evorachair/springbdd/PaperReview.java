package evorachair.springbdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PaperReview {

    @Given("I am a conference reviewer")
    public void iAmConferenceReviewer() {

    }

    @Given("there are submitted papers for review")
    public void submittedPapersForReview() {
        
    }

    @When("I review the papers")
    public void reviewPapers() {
        
    }

    @Then("I can provide feedback and make acceptance decisions")
    public void provideFeedbackAndMakeAcceptanceDecisions() {
        
    }

    @Given("I am reviewing a paper")
    public void reviewingPaper() {
        
    }

    @When("I provide feedback on the paper")
    public void provideFeedbackOnPaper() {
        
    }

    @Then("the feedback is recorded")
    public void feedbackRecorded() {
        
    }

    @Given("I have reviewed all the papers")
    public void reviewedAllPapers() {
        
    }

    @When("I make acceptance decisions")
    public void makeAcceptanceDecisions() {
        
    }

    @Then("the decisions are recorded")
    public void decisionsRecorded() {
        
    }
}