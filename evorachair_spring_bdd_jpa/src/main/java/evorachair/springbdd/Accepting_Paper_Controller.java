package evorachair.springbdd;

import java.awt.print.Paper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/papers")
public class Accepting_Paper_Controller<PaperService> {

    @Autowired
    public Accepting_Paper_Controller(PaperService paperService) {
    }

    @PostMapping("/accept")
    public ResponseEntity<String> acceptPaper(@RequestBody Paper paper) {
      
        
        // Return a success response
        return ResponseEntity.status(HttpStatus.CREATED).body("Paper accepted successfully");
    }
}
