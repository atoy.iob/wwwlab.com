Feature: Accepting Papers
  As a conference organizer
  I want to accept submitted papers
  So that I can include them in the conference program

  Scenario: Accepting a Paper
    Given there is a submitted paper
    When I accept the paper
    Then the paper is marked as accepted
    And the paper is included in the conference program