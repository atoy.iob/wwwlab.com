Feature: Reviewing Papers
  As a conference reviewer
  I want to review submitted papers
  So that I can provide feedback and make acceptance decisions

  Scenario: Reviewing Papers
    Given I am a conference reviewer
    And there are submitted papers for review
    When I review the papers
    Then I can provide feedback and make acceptance decisions

  Scenario: Providing Feedback
    Given I am reviewing a paper
    When I provide feedback on the paper
    Then the feedback is recorded

  Scenario: Making Acceptance Decisions
    Given I have reviewed all the papers
    When I make acceptance decisions
    Then the decisions are recorded