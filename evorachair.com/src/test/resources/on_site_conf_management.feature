Feature: On-site Conference Management
  As a conference organizer
  I want to manage the on-site operations of the conference
  So that the event runs smoothly

  Scenario: Setting Up Conference Venue
    Given I am a conference organizer
    When I set up the conference venue
    Then the venue is prepared for the event

  Scenario: Managing Registration Desk
    Given the conference venue is set up
    And participants have arrived for registration
    When I manage the registration desk
    Then participants receive their conference materials

  Scenario: Monitoring Conference Sessions
    Given the conference sessions have started
    When I monitor the conference sessions
    Then I ensure the sessions run as scheduled

  Scenario: Coordinating Breaks and Meals
    Given the conference sessions are ongoing
    When I coordinate breaks and meals
    Then participants are provided with refreshments and meals

  Scenario: Handling Technical Issues
    Given there are technical issues during the conference
    When I handle the technical issues
    Then I ensure they are resolved promptly

  Scenario: Closing the Conference
    Given the conference has come to an end
    When I close the conference
    Then I wrap up the event and gather feedback