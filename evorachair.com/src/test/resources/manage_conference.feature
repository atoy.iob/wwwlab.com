Feature: Managing Conference Registration
  As a conference organizer
  I want to manage conference registration
  So that participants can register for the conference

  Scenario: Opening Conference Registration
    Given I am a conference organizer
    When I open conference registration
    Then participants can register for the conference

  Scenario: Setting Registration Fees
    Given conference registration is open
    When I set the registration fees
      | Registration Type | Fee  |
      | Regular           | $100 |
      | Student           | $50  |
    Then the registration fees are set

  Scenario: Processing Registrations
    Given conference registration is open and fees are set
    When participants register for the conference
      | Participant Name | Registration Type |
      | John Doe         | Regular           |
      | Jane Smith       | Student           |
    Then the registrations are processed

  Scenario: Sending Confirmation Emails
    Given conference registrations are processed
    When I send confirmation emails to participants
    Then participants receive confirmation emails