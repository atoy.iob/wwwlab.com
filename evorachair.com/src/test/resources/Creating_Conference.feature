Feature: Creating a Conference
  As a conference organizer
  I want to create a conference event
  So that participants can register and attend

  Scenario: Creating a New Conference
    Given I am a conference organizer
    When I create a new conference
    Then the conference details are saved

  Scenario: Providing Conference Information
    Given I have created a new conference
    When I provide the conference details
      | Name                            | International Conference on Software Engineering |
      | Theme                           | Advancing Software Engineering Practices       |
      | Dates                           | October 10-12, 2023                             |
      | Location                        | City Convention Center                          |
    Then the conference details are updated

  Scenario: Displaying Conference Information
    Given the conference details are provided
    When I view the conference details
    Then I should see the following information
      | Name                            | International Conference on Software Engineering |
      | Theme                           | Advancing Software Engineering Practices       |
      | Dates                           | October 10-12, 2023                             |
      | Location                        | City Convention Center 