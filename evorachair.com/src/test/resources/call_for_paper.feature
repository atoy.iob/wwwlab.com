Feature: Call for Paper
  As a conference organizer
  I want to announce a call for paper
  So that researchers can submit their papers

  Scenario: Announcing Call for Paper
    Given I am a conference organizer
    When I announce a call for paper
      | Conference Name               | International Conference on Software Engineering |
      | Submission Deadline           | June 30, 2023                                    |
      | Maximum Number of Submissions | 100                                              |
    Then the call for paper is announced

  Scenario: Submitting Papers
    Given the call for paper is announced
    When researchers submit their papers
      | Researcher       | Paper Title                     |
      | John Doe         | Advances in Software Testing     |
      | Jane Smith       | Machine Learning for Data Mining |
    Then the papers are submitted

  Scenario: Reviewing Papers
    Given papers are submitted
    When reviewers review the papers
      | Reviewer         | Paper Title                     | Review           |
      | Sarah Johnson    | Advances in Software Testing     | Accepted         |
      | Michael Williams | Machine Learning for Data Mining | Needs Revision   |
    Then the papers are reviewed