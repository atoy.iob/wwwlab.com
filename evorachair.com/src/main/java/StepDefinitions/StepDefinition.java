package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinition<EvoraChairService> {
		
			
    @Given("I am on the login page")
    public void givenIAmOnTheLoginPage() {
        // Implementasi logika untuk mengakses halaman login EvoraChair
    }

    @When("I enter valid credentials")
    public void whenIEnterValidCredentials() {
        // Implementasi logika untuk memasukkan kredensial yang valid
    }

    @Then("I should be redirected to the dashboard")
    public void thenIShouldBeRedirectedToTheDashboard() {
        // Implementasi logika untuk memeriksa pengalihan ke halaman dashboard
    }

    // Metode lain sesuai dengan skenario pengujian dalam file fitur
}